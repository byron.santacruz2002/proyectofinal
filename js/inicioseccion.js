document.querySelector('#boton').addEventListener('click', inicioSeccion);

function inicioSeccion(){
    var sCorreo = '';
    var sContrasena = '';
    var bAcceso = false;
    sCorreo = document.querySelector('#correo').value;
    sContrasena = document.querySelector('#contraseña').value; 
    bAcceso = validar(sCorreo,sContrasena);
    if(bAcceso == true){
        ingresar();
    }else{
        alert("Contraseña o usuario incorrecto");
    }
}
function ingresar(){
    var rol = sessionStorage.getItem('rolUsuarioActivo');
    switch(rol){
        case '1':
            window.location.href='admin-inicio.html';
        break;
        case '2':
            window.location.href='estudiante.html';
        break;
        case '3':
            window.location.href='profesor.html';
        break;
    }
}
