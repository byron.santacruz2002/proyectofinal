function obtenerListaUsuarios(){
    var ListaUsuarios = JSON.parse(localStorage.getItem('ListaUsuariosLs'));
    if(ListaUsuarios == null){
        ListaUsuarios=
        [
            ['1','Nohelia','Santacruz','nohelia.santacruz2002@gmail.com','123','2022-11-10','2'],
            ['2','Byron','Santacruz','byron.santacruz2002@gmail.com','Bass@131558','2022-22-10','1'],
            ['3','Nohelia','Santacruz','pattyquirozpalma@live.uleam.edu.ec','12345','2022-11-02','3']
        ]
    }
    return ListaUsuarios;
}
function validar(pCorreo , pContrasena){
    var ListaUsuarios= obtenerListaUsuarios();
    var bAcceso = false;

    for(var i = 0; i < ListaUsuarios.length; i++){
        if(pCorreo == ListaUsuarios[i][3] && pContrasena == ListaUsuarios[i][4]){
            bAcceso=true;
            sessionStorage.setItem('usuarioActivo', ListaUsuarios[i][1] + '' + ListaUsuarios[i][2]);
            sessionStorage.setItem('rolUsuarioActivo',ListaUsuarios[i][6]);     
        }
    }
    return bAcceso;
}