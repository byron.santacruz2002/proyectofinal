function verificarPasswords() {
    pass1 = document.getElementById('pass1');
    pass2 = document.getElementById('pass2');      
    if (pass1.value != pass2.value) {
        document.getElementById("error").classList.add("mostrar");
        return false;
    }
    else {
        document.getElementById("error").classList.remove("mostrar");
        document.getElementById("ok").classList.remove("ocultar");
        setTimeout(function() {
            window.location.href='Login.html';
        }, 4000);
        return true;
    }
}

